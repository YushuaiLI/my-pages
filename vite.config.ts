import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base: '/my_pages/',
  plugins: [vue()],
  build: {
    outDir: 'dist',
    assetsDir: 'static',
  }
})
